<?php

namespace App\Http\Controllers;

use App\Car;
use App\Code;
use App\Driver;
use Illuminate\Http\Request;

class CarController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $car = Car::create($request->car);
        $driver = new Driver;
        $driver->name = $request->driver["name"];
        $driver->last_name = $request->driver["last_name"];
        $driver->dni = $request->driver["dni"];
        $driver->email = $request->driver["email"];
        $driver->guest_count = $request->driver["guest_count"];
        $driver->car_id = $car->id;
        $driver->save();
        if($driver->guest_count !=null){
            for ($i = 0;$i <= $driver->guest_count;$i++){
                $code = new Code;
                $code->code = uniqid();
                $code->driver_id = $driver->id;
                $code->save();
            }
            return $driver->codes;
        }
        return 'done';
    }
}
