<?php

namespace App\Http\Controllers;

use App\Car;
use App\Code;
use Illuminate\Http\Request;

class CodeController extends Controller
{

    public function find(Request $request)
    {
        $code = Code::where('code',$request->code)->first();
        //$car = Car::find($code)
        return ['driver'=>$code->driver,'car'=>$code->driver->car];
    }
}
