<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $guarded = ['id', 'created_at', 'updated_at'];

    public function driver(){
        return $this->hasOne(Driver::class);
    }
}
