<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver extends Model
{
    protected $fillable = [];

    public function car(){
        return $this->belongsTo(Car::class);
    }

    public function codes(){
        return $this->hasMany(Code::class);
    }
}
