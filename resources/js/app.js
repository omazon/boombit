require('./bootstrap');
window.Vue = require('vue');

import VeeValidate from 'vee-validate'

Vue.component('form-send', require('./components/FormSend.vue'));
Vue.component('find-code', require('./components/FindCode.vue'));

Vue.use(VeeValidate);
const app = new Vue({
    el: '#app'
});
