<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Best Cars Fest 2018</title>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
        <!--Icons-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css">
        <!-- Styles -->
        <link rel="stylesheet" href="{{secure_asset("css/app.css")}}">
    </head>
    <body>
    @if (Route::has('login'))
        <div class="top-right links">
            @auth
                <a class="text-white" href="{{ secure_url('/home') }}">Home</a>
            @else
                <a class="text-white" href="{{ route('login') }}">Login</a>
            @endauth
        </div>
    @endif
       <section class="header-banner text-center">
            <div class="header-banner-black d-flex justify-content-center align-items-center flex-column">
                <h2 class="display-1">Best Cars Fest</h2>
                <h3 class="display-3">Managua 2018</h3>
            </div>
       </section>
       <section class="container my-5">
           <p class="font-size-30">La exhibición de autos más importante del mundo arranca el dia 30 de noviembre en Metrocentro, con la presentación de más de 500 vehículos para todos los gustos.</p>
       </section>
    <section class="gallery mb-5">
        <h3 class="display-3">Galeria</h3>
        <div class="d-flex flex-wrap justify-content-center">
            <img src="https://loremflickr.com/320/240" alt="">
            <img src="https://loremflickr.com/320/240" alt="">
            <img src="https://loremflickr.com/320/240" alt="">
            <img src="https://loremflickr.com/320/240" alt="">
            <img src="https://loremflickr.com/320/240" alt="">
            <img src="https://loremflickr.com/320/240" alt="">
            <img src="https://loremflickr.com/320/240" alt="">
            <img src="https://loremflickr.com/320/240" alt="">
        </div>
    </section>
    <section id="app">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <p class="font-size-24">
                        Registrate y asiste a una oportunidad unica
                    </p>
                </div>
                <div class="col-sm-8">
                    <form-send></form-send>
                </div>
            </div>
        </div>
    </section>
    <script src="{{secure_asset('js/app.js')}}"></script>
    </body>
</html>
